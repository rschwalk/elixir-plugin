# elixir_plugin.py
#
# Copyright 2019 Richard Schwalk <rschwalk@rschwalk.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import gi
import sys

from gi.repository import GObject
from gi.repository import GtkSource
from gi.repository import Gio
from gi.repository import Ide

_ = Ide.gettext

class ElixirAppAddin(GObject.Object, Ide.ApplicationAddin):

    def do_load(self, application):
        print("hello")
        manager = GtkSource.LanguageManager.get_default()
        paths = manager.get_search_path()
        paths.append('resources:///plugins/language-specs/')
        manager.set_search_path(paths)

    def do_unload(self, application):
        print("goodbye")





# SEVERITY_MAP = {
#     1: Ide.DiagnosticSeverity.WARNING,
#     2: Ide.DiagnosticSeverity.ERROR
# }


# class ESLintDiagnosticProvider(Ide.Object, Ide.DiagnosticProvider):

#     def do_diagnose_async(self, file, file_content, lang_id, cancellable, callback, user_data):
#         self.diagnostics_list = []

#         task = Gio.Task.new(self, cancellable, callback)
#         task.diagnostics_list = []

#         severity = Ide.DiagnosticSeverity.WARNING
#         error_message = 'Diagnostic example'

#         start = Ide.Location.new(file, 0, 0)
#         diagnostic = Ide.Diagnostic.new(severity, error_message, start)
#         task.diagnostics_list.append(diagnostic)

#         task.return_boolean(True)


#     def do_diagnose_finish(self, result):
#         if result.propagate_boolean():
#             diagnostics = Ide.Diagnostics()
#             for diag in result.diagnostics_list:
#                 diagnostics.add(diag)
#             return diagnostics


##########

#TODO: remove unnecesary prints

_TYPE_KEYWORD = 1
_TYPE_FUNCTION = 2
_TYPE_MODULE = 3
_TYPE_STRUCT = 4
_TYPE_PARAM = 5
_TYPE_PROTOCOL = 6
_TYPE_IMPLEMENTATION = 7
_TYPE_EXCEPTION = 8

_ICONS = {
    _TYPE_KEYWORD: 'lang-function-symbolic',
    _TYPE_FUNCTION: 'lang-function-symbolic',
    _TYPE_MODULE: 'lang-method-symbolic',
    _TYPE_STRUCT: 'lang-struct-symbolic',
    _TYPE_PARAM: 'lang-variable-symbolic',
    _TYPE_PROTOCOL: 'lang-include-symbolic',
    _TYPE_IMPLEMENTATION: 'lang-define-symbolic',
    _TYPE_EXCEPTION:'lang-enum-symbolic',
}

debug = False

class ElixirCompletionProvider(Ide.Object, Ide.CompletionProvider):
    context = None
    current_word = None
    results = None
    thread = None
    line = -1
    line_offset = -1

    def do_get_title(self):
        return 'Elixir'

    def do_get_icon(self):
        return None

    def do_get_priority(self, context):
        return 200

    def do_load(self, context):
        pass

    def do_populate_async(self, context, cancellable, callback, data):
        """
        @context: an Ide.CompletionContext containing state for the completion request
        @cancellable: a Gio.Cancellable that can be used to cancel the request
        @callback: a callback to execute after proposals are generated
        @data: closure data (unnecessary for us since PyGObject handles it)
        """

        task = Ide.Task.new(self, None, callback)
        task.set_name('elixir-completion')

        self.current_word = context.get_word()
        print("Current %s" % self.current_word)
        self.current_word_lower = self.current_word.lower()

        _, iter, _ = context.get_bounds()

        buffer = context.get_buffer()

        begin, end = buffer.get_bounds()


        task.filename = os.path.dirname(buffer.get_file().get_path())
        task.line = iter.get_line()
        task.line_offset = iter.get_line_offset()
        #if task.line_offset > 0:
        #    task.line_offset -= 1
        task.text = buffer.get_text(begin, end, True)

        request = "suggestions"
        alchemist_script = ""
        ansi = True
        elixir_otp_src = ""


        cwd = task.filename
        line = task.line + 1
        column = task.line_offset + 1 + len(self.current_word)
        source = task.text

        print("cwd: %s, line: %s, col: %s" % (cwd, line, column))
        print(source)

        task.proposals = ElixirProposals()

        alchemist_script = "/home/rschwalk/.local/share/gnome-builder/plugins/elixir_sense/run.exs"

        sense = ElixirSenseClient(debug=debug, cwd=cwd, ansi=ansi, elixir_sense_script=alchemist_script, elixir_otp_src=elixir_otp_src)
        response = sense.process_command(request, source, line, column)

        for item in response:
            prop = ElixirProposal(item)
            task.proposals.append(prop)

        # Complete the task so that the completion engine calls us back at
        # do_populate_finish() to complete the operation.
        #task.return_boolean(True)

        task.return_object(task.proposals)

    def do_populate_finish(self, task):
        """
        @task: the task we created in do_populate_async()

        The goal here is to copmlete the operation by returning our Gio.ListModel
        back to the caller.
        """
        #if task.propagate_boolean():
        #    return task.proposals

        return task.propagate_object()


    def do_display_proposal(self, row, context, typed_text, proposal):
        """
        We need to update the state of @row using the info from our proposal.
        This is called as the user moves through the result set.

        @row: the Ide.CompletionListBoxRow to display
        @context: the Ide.CompletionContext
        @typed_text: what the user typed
        @proposal: the proposal to display using @row
        """

        #prop = proposal.prop_dict

        row.set_icon_name(proposal.get_icon_name())
        row.set_left(proposal.get_type())
        row.set_center_markup(proposal.get_markup(typed_text))
        row.set_right(proposal.get_text())


    def do_activate_proposal(self, context, proposal, key):
        """
        @proposal: the proposal to be activated
        @key: the Gdk.EventKey representing what the user pressed to activate the row
              you may want to use this to alter what is inserted (such as converting
              . to -> in a C/C++ language provider).

        This is where we do the work to insert the proposal. You may want to
        check out the snippet engine to use snippets instead, as some of the
        providers do which auto-complete parameters.
        """

        buffer = context.get_buffer()
        view = context.get_view()

        _, begin, end = context.get_bounds()

        snippet = proposal.get_snippet()

        # Start a "user action" so that all the changes are coalesced into a
        # single undo action.
        buffer.begin_user_action()
        buffer.delete(begin, end)

        view.push_snippet(snippet, begin)

        # Now insert our proposal
        #word = proposal.get_label()
        #buffer.insert(begin, word, len(word))

        # Complete the user action
        buffer.end_user_action()

    def do_is_trigger(self, iter, ch):
        if ch == '.':
            buffer = iter.get_buffer()
            if buffer.iter_has_context_class(iter, 'string') or \
               buffer.iter_has_context_class(iter, 'comment'):
                return False
            return True
        return False

    def do_refilter(self, context, proposals):
        """
        If you can refilter the results based on updated typed text, this
        is where you would adjust @proposals to do that. @proposals is the
        Gio.ListModel returned from do_populate_finish().
        """
        word = context.get_word().lower()
        proposals.refilter(word)
        return True



class ElixirProposals(GObject.Object, Gio.ListModel):
    items = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.items = []

    def append(self, item):
        self.items.append(item)

    def do_get_item_type(self):
        return type(Ide.CompletionProposal)

    def do_get_n_items(self):
        return len(self.items)

    def do_get_item(self, position):
        return self.items[position]

    def refilter(self, casefold):
        old_len = len(self.items)
        old_items = self.items
        self.items = []
        for i in old_items:
            label = i.get_label()
            match, priority = Ide.Completion.fuzzy_match(label, casefold)
            if match:
                self.items.append(i)
        #self.items.sort(key=lambda x: x[1])
        self.items_changed(0, old_len, len(self.items))

class ElixirProposal(GObject.Object, Ide.CompletionProposal):
    def __init__(self, proposal):
        super().__init__()
        self.prop_dict = proposal

    # TODO: use properties
    def get_kind(self):
        kind = self.prop_dict["kind"]
        if kind =="f":
            return _TYPE_FUNCTION
        else:
            subtype = self.get_type()
            if subtype == "exception":
                return _TYPE_EXCEPTION
            else:
                return _TYPE_MODULE

        return _TYPE_PROTOCOL

    def get_icon_name(self):
        return _ICONS.get(self.get_kind(), None)

    def get_label(self):
        return self.prop_dict["word"]

    def get_markup(self, casefold):
        markup = Ide.Completion.fuzzy_highlight(self.get_label(), casefold)

        return markup

    def get_type(self):
        return self.prop_dict["menu"]

    def get_text(self):
        return self.prop_dict["sig"]

    def get_comment(self):
        pass

    def get_params(self):
        return self.prop_dict["param"].split(",")

    def get_snippet(self):
        snippet = Ide.Snippet.new(None, None)
        snippet.add_chunk(Ide.SnippetChunk(spec=self.get_label()))

        # Add parameter completion for functions.
        kind = self.get_kind()
        if kind == _TYPE_FUNCTION:
            snippet.add_chunk(Ide.SnippetChunk(text='(', text_set=True))
            params = self.get_params()
            print("params: %s" % params)
            if params:
                tab_stop = 0
                for param in params[:-1]:
                    tab_stop += 1
                    if param.startswith('param '):
                        param = param[6:]
                    snippet.add_chunk(Ide.SnippetChunk(text=param, text_set=True, tab_stop=tab_stop))
                    snippet.add_chunk(Ide.SnippetChunk(text=', ', text_set=True))
                tab_stop += 1
                snippet.add_chunk(Ide.SnippetChunk(text=params[-1], text_set=True, tab_stop=tab_stop))
            snippet.add_chunk(Ide.SnippetChunk(text=')', text_set=True))

        return snippet


import os
import tempfile
import re
import pprint
import subprocess, shlex
import select, socket
import time
try :
    import syslog
except ImportError:
    pass
import struct
import erl_terms
import errno

class ElixirSenseClient:


    def __init__(self, **kw):
        self._debug = kw.get('debug', False)
        self._cwd = kw.get('cwd', '')
        self.__create_tmp_dir()
        self._cwd = self.get_project_base_dir()
        self._ansi = kw.get('ansi', True)
        self._alchemist_script = kw.get('elixir_sense_script', None)
        self._elixir_otp_src = kw.get('elixir_otp_src', None)
        self.re_erlang_module = re.compile(r'^(?P<module>[a-z])')
        self.re_elixir_src = re.compile(r'.*(/elixir.*/lib.*)')
        self.re_erlang_src = re.compile(r'.*otp.*(/lib/.*\.erl)')
        self.sock = None


    def __create_tmp_dir(self):
        dir_tmp = self._get_tmp_dir()
        if os.path.exists(dir_tmp) == False:
            os.makedirs(self._get_tmp_dir())

    def process_command(self, request, source, line, column):
        self._log('column: %s' % column)
        self._log('line: %s' % line)
        self._log('source: %s' % source)
        py_struct = {
                'request_id': 1,
                'auth_token': None,
                'request': request,
                'payload': {
                    'buffer': source,
                    'line': int(line),
                    'column': int(column)
                    }
                }

        req_erl_struct = erl_terms.encode(py_struct)

        sock = self.__get_socket()

        try:
            resp_erl_struct = self._send_command(sock, req_erl_struct)
        except Exception as e:
            return 'error:%s' % e

        rep_py_struct = erl_terms.decode(resp_erl_struct)
        if rep_py_struct['error']:
            return 'error:%s' % rep_py_struct['error']
        self._log('ElixirSense: %s' % rep_py_struct)
        if request == "suggestions":
            print(rep_py_struct['payload'][0])
            return self.to_suggestions(rep_py_struct['payload'])
            #return rep_py_struct['payload']
        elif request == "docs":
            if rep_py_struct['payload']['docs']:
                return rep_py_struct['payload']['docs']['docs']
            return rep_py_struct['payload']
        elif request == 'definition':
            return self.to_vim_definition(rep_py_struct['payload'])

    def __get_socket(self):
        if self.sock:
            return self.sock
        server_log = self._get_running_server_log()
        if server_log == None:
            server_log = self._create_server_log()
            self._run_alchemist_server(server_log)

        connection = self._extract_connection_settings(server_log)
        if connection == None:
            self._run_alchemist_server(server_log)
            connection = self._extract_connection_settings(server_log)

        sock = self._connect(connection)
        if sock == None:
            self._run_alchemist_server(server_log)
            connection = self._extract_connection_settings(server_log)
            if connection == None:
                self._log("Couldn't find the connection settings from server_log: %s" % (server_log))
                return  None
            sock = self._connect(connection)
        self.sock = sock
        return self.sock

    def to_vim_definition(self, definition):
        if definition['found']:
            line = definition['line']
            filename = definition['file']
            if self._is_readable(filename):
                return "%s:%s" % (filename, line)

            filename = self._find_elixir_erlang_src(filename)
            return "%s:%i" %(filename, 0)
        else:
            return "definition_not_found"

    def to_suggestions(self, suggestions):

        result = []
        prefix_module = ''
        hint = suggestions[0]
        if '.' in hint['value']:
            prefix_module = '.'.join(hint['value'].split('.')[:-1]) + '.'

        suggestions = sorted(suggestions[1:], key=lambda s : dict.get(s, 'name', ''))
        #TODO: rewrite to make more sense
        for s in suggestions:
            #TODO: handle all types
            if s['type'] == 'hint':
                continue
            if s['type'] == 'module':
                mtype = s['subtype'] or s['type']
                if ('%s.' % s['name']) == prefix_module:
                    word = "%s" % (prefix_module)
                else:
                    if re.match(r'.*%s.$' %(s['name']), prefix_module):
                        word = prefix_module
                    else:
                        #word = "%s%s" % (prefix_module, s['name'])
                        word = s['name']
                if self.re_erlang_module.match(s['name']):
                    word = self.__erlang_pad(word)
                    s['name'] = self.__erlang_pad(s['name'])
                info = s['summary']
                result.append(self.__suggestion_line('m', word, '', s['name'], mtype, info))
            if s['type'] == 'function':
                #if ('%s.' % s['origin'][((len(prefix_module) -1)*-1):]) == prefix_module:
                #    word = '%s%s' % (prefix_module, s['name'])
                #else:
                #    word = '%s.%s' % (s['origin'], s['name'])
                word = s['name']
                if word[0] == ':':
                    args = '%s/%s' % (s['name'], s['arity'])
                else:
                    if s['args'] == None:
                        s['args'] = ''
                    param = s['args']
                    sig = '%s(%s)' % (s['name'], ", ".join(s['args'].split(',')))
                info = s['summary']
                if s['spec'] and s['summary'] != '':
                    info = '%s\n%s' % (s['spec'].strip(), s['summary'].strip())
                elif s['spec']:
                    info = s['spec'].strip()

                result.append(self.__suggestion_line('f', word, param, sig, s['origin'], info))

        return result

    def __suggestion_line(self, kind, word, param, sig, menu, info):
        info = info.strip().replace('\n', "<n>")
        return {"kind": kind, "word": word, "param": param, "sig": sig, "menu": menu, "info": info}

    def __erlang_pad(self, module):
        if self.re_erlang_module.match(module):
            return ':%s' % module
        else:
            return module

    def _log(self, text):
        if self._debug == False:
            return

        f = open("/tmp/log.log", "a")
        f.write("%s\n" % text.encode('utf8'))
        f.close()

        #syslog.openlog("alchemist_client")
        #syslog.syslog(syslog.LOG_ALERT, text)

    def _get_path_unique_name(self, path):
        """
        >>> alchemist = ElixirSenseClient()
        >>> alchemist._get_path_unique_name("/Users/milad/dev/ex_guard/")
        'zS2UserszS2miladzS2devzS2ex_guard'
        """
        return os.path.abspath(path).replace("/", "zS2")

    def _create_server_log(self):
        dir_tmp = self._get_tmp_dir()
        log_tmp = "%s/%s" % (dir_tmp, self._cwd.replace("/", "zS2"))
        if os.path.exists(dir_tmp) == False:
            os.makedirs(dir_tmp)

        if os.path.exists(log_tmp) == False:
            return log_tmp

        return None

    def _get_running_server_log(self):
        dir_tmp = self._get_tmp_dir()
        log_tmp = "%s/%s" % (dir_tmp, self._get_path_unique_name(self._cwd))
        self._log("Load server settings from: %s" % (log_tmp))
        if os.path.exists(dir_tmp) == False:
            return None

        if os.path.exists(log_tmp) == True:
            return log_tmp

        return None

    def _run_alchemist_server(self, server_log):
        """
        execute alchemist server and wait until it has printed a line
        into STDOUT
        """
        alchemist_script = self._alchemist_script
        if os.path.exists(alchemist_script) == False:
            raise Exception("alchemist script does not exist in (%s)" % alchemist_script)
        alchemist_script = "elixir %s unix 0 dev" % alchemist_script
        self._log(alchemist_script)
        arg = shlex.split(alchemist_script)
        log_file = open(server_log, "w")
        subprocess.Popen(arg, stdout=log_file, stderr=log_file, stdin=log_file, cwd=self._cwd)
        for t in range(0, 50):
            time.sleep(0.1)
            r = open(server_log).readlines()
            if len(r) > 0:
                break

    def _connect(self, host_port):
        if host_port == None: return None
        (host, port) = host_port
        if isinstance(port, str):
            sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            host_port = port
        else:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            sock.connect(host_port)
        except socket.error as e:
            self._log("Can not establish connection to %s, error: %s" % (host_port, e))
            return None

        return sock

    def _send_command(self, sock, cmd):
        packer = struct.Struct('!I')
        packed_data = packer.pack(len(cmd))
        try:
            if sock is None: raise Exception("Socket is not available.")
            sock.sendall(packed_data + cmd)
            return self._sock_readlines(sock)
        except socket.error as e:
            self.sock = None
            self._log("Exception in communicating with server: %s" % e)
            if e.errno == 35:
                raise Exception("reached 10 sec timeout, error:Resource temporarily unavailable")
            elif e.errno == errno.EPIPE:
                raise Exception("Lost connection to Server. Try again, error:Resource temporarily unavailable")
            else:
                raise e

        self._log("response for %s: %s" % (cmd.split(" ")[0], result.replace('\n', '\\n')))
        return ''

    def _find_elixir_erlang_src(self, filename):
        if self._is_readable(filename):
            return filename
        if self.re_elixir_src.match(filename):
            elixir_src_file = "%s/elixir/lib/%s" % (self._elixir_otp_src, self.re_elixir_src.match(filename).group(1))
            if self._is_readable(elixir_src_file):
                return os.path.realpath(elixir_src_file)
        elif self.re_erlang_src.match(filename):
            erlang_src_file = "%s/otp/%s" % (self._elixir_otp_src, self.re_erlang_src.match(filename).group(1))
            if self._is_readable(erlang_src_file):
                return os.path.realpath(erlang_src_file)
        return filename

    def _find_module_line(self, filename, module):
        return self._find_pattern_in_file(
                filename,
                ["defmodule %s" % module, "-module(%s)." % module[1:]])

    def _find_function_line(self, filename, function):
        return self._find_pattern_in_file(
                filename,
                ["def %s" % function, "defp %s" % function, "-spec %s" % function])

    def _find_pattern_in_file(self, filename, patterns):
        if not os.path.isfile(filename) or not os.access(filename, os.R_OK):
            return 1
        lines = open(filename, "r").readlines()
        for line_num, line_str in enumerate(lines):
            matched_p = list(filter(lambda p: p in line_str, patterns))
            if len(matched_p) > 0:
                return line_num + 1
        return 1

    def _is_readable(self, filename):
        if os.path.isfile(filename) and os.access(filename, os.R_OK):
            return True
        return False

    def _sock_readlines(self, sock, recv_buffer=4096, timeout=10):
        sock.setblocking(0)
        packet_size = -1

        select.select([sock], [], [], timeout)
        data = sock.recv(recv_buffer)
        (packet_size, ) = struct.unpack('!I', data[:4])
        buf = data[4:]
        while len(buf) < packet_size:
            select.select([sock], [], [], timeout)
            data = sock.recv(recv_buffer)
            buf = buf + data
        return buf

    def _extract_connection_settings(self, server_log):
        """
        >>> alchemist = ElixirSenseClient()
        >>> server_log = "t/fixtures/alchemist_server/valid.log"
        >>> print(alchemist._extract_connection_settings(server_log))
        ('localhost', '/tmp/elixir-sense-1502654288590225000.sock')

        >>> server_log = "t/fixtures/alchemist_server/invalid.log"
        >>> print(alchemist._extract_connection_settings(server_log))
        None
        """
        for line in open(server_log, "r").readlines():
            self._log(line)
            match = re.search(r'ok\:(?P<host>\w+):(?P<port>.*\.sock)', line)
            if match:
                (host, port) = match.groups()
                try :
                    return (host, int(port))
                except :
                    return (host, port)
        return None

    def _get_tmp_dir(self):
        """
        >>> alchemist = ElixirSenseClient()
        >>> os.environ['TMPDIR'] = '/tmp/foo01/'
        >>> alchemist._get_tmp_dir()
        '/tmp/foo01/alchemist_server'
        >>> del os.environ['TMPDIR']
        >>> os.environ['TEMP'] = '/tmp/foo02/'
        >>> alchemist._get_tmp_dir()
        '/tmp/foo02/alchemist_server'
        >>> del os.environ['TEMP']
        >>> os.environ['TMP'] = '/tmp/foo03/'
        >>> alchemist._get_tmp_dir()
        '/tmp/foo03/alchemist_server'
        >>> del os.environ['TMP']
        >>> alchemist._get_tmp_dir() == tempfile.tempdir #TODO: revert
        False
        """
        for var in ['TMPDIR', 'TEMP', 'TMP']:
            if var in os.environ:
                return os.path.abspath("%s/alchemist_server" % os.environ[var])
        if tempfile.tempdir != None:
            return os.path.abspath("%s/alchemist_server" % tempfile.tempdir)

        return "%s/alchemist_server" % "/tmp"

        pass

    def get_project_base_dir(self, running_servers_logs=None):
        """
        >>> #prepare the test env
        >>> tmp_dir = tempfile.mkdtemp()
        >>> p01_dir = os.path.join(tmp_dir, "p01")
        >>> os.mkdir(p01_dir)
        >>> p01_lib_dir = os.path.join(p01_dir, "lib")
        >>> os.mkdir(p01_lib_dir)
        >>> p01_log = p01_dir.replace("/", "zS2")

        >>> #detect that base dir is already running
        >>> alchemist = ElixirSenseClient(cwd=p01_dir)
        >>> alchemist.get_project_base_dir([p01_log]) == p01_dir
        True
        >>> #since server is running on base dir, if lib dir is given, should return base dir
        >>> alchemist = ElixirSenseClient(cwd=p01_lib_dir)
        >>> alchemist.get_project_base_dir([p01_log]) == p01_dir
        True
        >>> #if given dir is out of base dir, should return the exact dir
        >>> alchemist = ElixirSenseClient(cwd=tmp_dir)
        >>> alchemist.get_project_base_dir([p01_log]) == tmp_dir
        True
        >>> #since there is no running server, lib dir is detected as base dir
        >>> alchemist = ElixirSenseClient(cwd=p01_lib_dir)
        >>> alchemist.get_project_base_dir([]) == p01_lib_dir
        True
        >>> #prepare mix test
        >>> open(os.path.join(p01_dir, "mix.exs"), 'a').close()
        >>> #should find mix.exs recursively and return base dir
        >>> alchemist = ElixirSenseClient(cwd=p01_lib_dir)
        >>> alchemist.get_project_base_dir([]) == p01_dir
        True
        >>> #find directory of parent when running inside a nested project
        >>> apps = os.path.join(p01_dir, "apps")
        >>> os.mkdir(apps)
        >>> nested = os.path.join(apps, "nested_project")
        >>> os.mkdir(nested)
        >>> nested_lib = os.path.join(nested, "lib")
        >>> os.mkdir(nested_lib)
        >>> open(os.path.join(nested, "mix.exs"), 'a').close()
        >>> alchemist = ElixirSenseClient(cwd=nested_lib)
        >>> alchemist.get_project_base_dir([]) == p01_dir
        True
        """

        if running_servers_logs == None:
            running_servers_logs = os.listdir(self._get_tmp_dir())
        paths = self._cwd.split(os.sep)
        mix_dir = []
        for i in range(len(paths)):
            project_dir = os.sep.join(paths[:len(paths)-i])
            if not project_dir:
                continue
            log_tmp = "%s" % project_dir.replace("/", "zS2")
            if log_tmp in running_servers_logs:
                self._log("project_dir(matched): "+str(project_dir))
                return project_dir

            if os.path.exists(os.path.join(project_dir, "mix.exs")):
                mix_dir.append(project_dir)

        self._log("mix_dir: "+str(mix_dir))
        if len(mix_dir):
            return mix_dir.pop()

        return self._cwd


