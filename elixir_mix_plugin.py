#!/usr/bin/env python3

# elixir_mix_plugin.py
#
# Copyright 2020 Richard Schwalk <rschwalk@rschwalk.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

from gi.repository import Ide
from gi.repository import GObject
from gi.repository import Gio
from gi.repository import GLib

_ = Ide.gettext

# _ERROR_FORMAT_REGEX = (
                       #"^\*\*\s\("
                       #"(?<level>[\\(\\w\\[a-zA-Z0-9\\]\\s]+)\\)"
#                        "(?<filename>[a-zA-Z0-9\\+\\-\\.\\/_]+):"
#                        "(?<line>\\d+):"
#                        "(?<message>.*)"
#                       )

class ElixirBuildSystemDiscovery(Ide.SimpleBuildSystemDiscovery):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.props.glob = 'mix.exs'
        self.props.hint = 'elixir_mix_plugin'
        self.props.priority = -200

class ElixirBuildSystem(Ide.Object, Ide.BuildSystem):
    project_file = GObject.Property(type=Gio.File)

    def do_get_id(self):
        return 'mix'

    def do_get_display_name(self):
        return 'Elixir mix'

    def do_get_priority(self):
        return -200

#class ElixirPipelineStage(Ide.Object, Ide.PipelineStage):
#    pass


class ElixirPipelineAddin(Ide.Object, Ide.PipelineAddin):
    """
    The ElixirPipelineAddin is responsible for creating the necessary build
    stages and attaching them to phases of the build pipeline.
    """
    error_format_id = None
    launcher_handler = None

    # def _on_launcher_created_cb(self, pipeline, launcher):
        # Set RUSTFLAGS so that we can parse error formats
    #     pass

    # def do_prepare(self, pipeline):
    #      self.error_format_id = pipeline.add_error_format(_ERROR_FORMAT_REGEX,
    #                                                       GLib.RegexCompileFlags.OPTIMIZE |
    #                                                       GLib.RegexCompileFlags.CASELESS)
    #     self.launcher_handler = pipeline.connect('launcher-created',
    #                                              self._on_launcher_created_cb)

    def do_load(self, pipeline):
        context = self.get_context()
        build_system = Ide.BuildSystem.from_context(context)

        # Ignore pipeline unless this is a elixir project
        if type(build_system) != ElixirBuildSystem:
            return

        project_file = build_system.props.project_file

        if project_file.get_basename() != 'mix.exs':
            project_file = project_file.get_child('mix.exs')

        mix_exs = project_file.get_parent().get_path()
        # config = pipeline.get_config()
        # builddir = pipeline.get_builddir()
        # runtime = config.get_runtime()
        # config_opts = config.get_config_opts()

        # Fetch dependencies so that we no longer need network access
        # fetch_launcher = pipeline.create_launcher()
        # fetch_launcher.setenv('CARGO_TARGET_DIR', builddir, True)
        # fetch_launcher.push_argv(cargo)
        # fetch_launcher.push_argv('fetch')
        # fetch_launcher.push_argv('--manifest-path')
        # fetch_launcher.push_argv(cargo_toml)
        # self.track(pipeline.attach_launcher(Ide.PipelinePhase.DOWNLOADS, 0, fetch_launcher))

        # Now create our launcher to build the project
        build_launcher = pipeline.create_launcher()
        build_launcher.set_cwd(mix_exs)
        build_launcher.push_argv('mix')
        build_launcher.push_argv('compile')

        # if not pipeline.is_native():
        #     build_launcher.push_argv('--target')
        #     build_launcher.push_argv(pipeline.get_host_triplet().get_full_name())

        # if config.props.parallelism > 0:
        #     build_launcher.push_argv('-j{}'.format(config.props.parallelism))

        # if not config.props.debug:
        #     build_launcher.push_argv('--release')

        clean_launcher = pipeline.create_launcher()
        clean_launcher.set_cwd(mix_exs)
        clean_launcher.push_argv('mix')
        clean_launcher.push_argv('clean')

        build_stage = Ide.PipelineStageLauncher.new(context, build_launcher)
        #build_stage = ElixirPipelineStage()
        #build_stage.set_launcher(build_launcher)
        build_stage.set_name(_("Building project"))
        build_stage.set_clean_launcher(clean_launcher)
        build_stage.connect('query', self._query)
        self.track(pipeline.attach(Ide.PipelinePhase.BUILD, 0, build_stage))

    # def do_unload(self, pipeline):
    #     if self.error_format_id is not None:
    #         pipeline.remove_error_format(self.error_format_id)
    #         self.error_format_id = None

    #     if self.launcher_handler is not None:
    #         pipeline.disconnect(self.launcher_handler)
    #         self.launcher_handler = None

    def _query(self, stage, pipeline, targets, cancellable):
        # Always defer to cargo to check if build is needed
        stage.set_completed(False)

class ElixirBuildTarget(Ide.Object, Ide.BuildTarget):

    def do_get_install_directory(self):
        return None

    def do_get_display_name(self):
        return 'mix run'

    def do_get_name(self):
        return 'mix-run'

    def do_get_language(self):
        return 'elixir'


    def do_get_argv(self):
        return ['mix', 'run']

    def do_get_priority(self):
        return 0

class ElixirBuildTargetProvider(Ide.Object, Ide.BuildTargetProvider):

    def do_get_targets_async(self, cancellable, callback, data):
        task = Gio.Task.new(self, cancellable, callback)
        task.set_priority(GLib.PRIORITY_LOW)

        context = self.get_context()
        build_system = Ide.BuildSystem.from_context(context)

        if type(build_system) != ElixirBuildSystem:
            task.return_error(GLib.Error('Not elixir build system',
                                         domain=GLib.quark_to_string(Gio.io_error_quark()),
                                         code=Gio.IOErrorEnum.NOT_SUPPORTED))
            return

        task.targets = [ElixirBuildTarget()]
        task.return_boolean(True)

    def do_get_targets_finish(self, result):
        if result.propagate_boolean():
            return result.targets
